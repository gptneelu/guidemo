/**
 * 
 */
package edu.codecnetworks.guidemo.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * @author NEELU
 *
 */
public class MainWindow //implements ActionListener // actionlistener is an
													// interface
{
	/**
	 * 
	 */
	JFrame frame = new JFrame("Application Title");
	JPanel[] panel=new JPanel[4];
	JLabel[] label=new JLabel[3];
	JTextField[] textField=new JTextField[3];
	JButton submitButton=new JButton("submit");
	public MainWindow() 
	{ // modifications done on frame are OS dependent
		for(int i=0;i<label.length;i++)
		{
			label[i]=new JLabel();
			textField[i]=new JTextField(10);
		}
		
		label[0].setText("Enter Dog Name: ");
		label[1].setText("Enter Dog Height: ");
		label[2].setText("Enter Dog No_of_Teeth: ");
		
		for(int i=0;i<panel.length;i++)
		{
			panel[i]=new JPanel();
	
			
		}
		for(int i=0;i<label.length;i++)
		{
			panel[i].add(label[i]);
			panel[i].add(textField[i]);
			panel[i].setBackground(Color.pink);
		}
		panel[3].setLayout(new BoxLayout(panel[3],BoxLayout.Y_AXIS));
		panel[3].add(panel[0]);
		panel[3].add(panel[1]);
		panel[3].add(panel[2]);
		panel[3].add(submitButton);
		
		submitButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e)
			{
				// TODO Auto-generated method stub
				System.out.print("button pressed");
			}
			
		});


							// therefore getContenPane is used to interact
							// directly with JFrame
							// TODO Auto-generated constructor stub
		frame.getContentPane().add(panel[3]); 
		frame.setSize(300, 300); // hardware pixels
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}


}
